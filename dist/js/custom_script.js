function add_butaca(node){
  var clone = $('.butaca-selected').children().clone(true);
  clone.attr('id', 'b-card'+node.id());
  clone.find('#b-fila').text(node.data().fila);
  clone.find('#b-asiento').text(node.data().label);
  clone.find('.node-id').val(node.id());
  clone.find('.b-remove').attr('id', node.id());

  $('.butaca-body').append(clone);
}

function remove_butaca(node){
  $('#b-card'+node.id()).remove();
}

$('.b-remove').on('click', function(){
  var node = find_node(this.id);
  unselect_node(node);
});