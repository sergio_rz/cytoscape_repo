/**
 * Created by sergiow on 03-01-18.
 */
var counting = 0;
$(document).ready(function () {
  counting = 0;
  
  // DIBUJA LOS NODOS O ASIENTOS
  var cy = window.cy = cytoscape({
      container: $('.mapa'),

      minZoom: 3e-2,
      maxZoom: 0.1e1,
      boxSelectionEnabled: true,
      autounselectify: true,
      //selectionType: 'additive',
      autoungrabify: true, //avoid drag and drop

      layout: {
          name: 'preset',
          width: 600,
          fit: true,
        },

      style: customStyle,
      elements: {
          nodes: nodos,
          edges: [],
        },
      ready: function () {
          //
        },
    });

    // ACCION TRAS CLIC SOBRE NODOS O ASIENTOS
    cy.on('tap', function (evt) {
      node = evt.target;
      if (node === cy) return;
      if ( node.hasClass('seleccionado')) {
        unselect_node(node);
      } else {
        if ( is_selected(node) && counting < 5 ) {
          select_node(node);
        }
      }
    });
});

/*
GET DATA FROM NODES
var node     = evt.target;
var id       = node.id();
var label    = node.data().label;
var type     = node.data().NodeType;
var monto    = node.data().monto;
var cantidad = node.data().cantidad;
var moneda   = node.data().moneda;
var id_db    = node.data().id_db;
*/

function is_selected(node){
  type = node.data().NodeType;
  return type != 'butaca_reservada' && type != 'butaca_ocupada' && type != 'butaca_inactiva' && type != 'medios' && type != 'fila';
}

$('.cy-zoom-plus').on('click', zoom_plus);
$('.cy-zoom-minus').on('click', zoom_minus);
$('.move-left').on('click', move_left);
$('.move-up').on('click', move_up);
$('.move-down').on('click', move_down);
$('.move-right').on('click', move_right);
$('.cy-reset').on('click', reset);

function zoom_plus(){
  var czoom = cy.zoom();
  cy.zoom( czoom + 0.1);
};

function zoom_minus(){
  var czoom = cy.zoom();
  cy.zoom( czoom - 0.1);
};

function move_left(){
  var current_zoom = cy.zoom();
  var position = cy.pan();
  var new_position = cy.pan({
    x: position.x + 40,
    y: position.y
  });
  cy.viewport( current_zoom, new_position);
};

function move_up(){
  var current_zoom = cy.zoom();
  var position = cy.pan();
  var new_position = cy.pan({
    x: position.x,
    y: position.y + 40
  });
  cy.viewport( current_zoom, new_position);
}

function move_down(){
  var current_zoom = cy.zoom();
  var position = cy.pan();
  var new_position = cy.pan({
    x: position.x,
    y: position.y - 40
  });
  cy.viewport( current_zoom, new_position);
}

function move_right(){
  var current_zoom = cy.zoom();
  var position = cy.pan();
  var new_position = cy.pan({
    x: position.x - 40,
    y: position.y
  });
  cy.viewport( current_zoom, new_position);
}

function reset(){
  cy.reset();
  cy.fit();
}

function select_node(node){
  node.addClass('seleccionado');
  counting += 1 ;
  try{add_butaca(node)}catch (e){
    console.log('You must create a js function called add_butaca to catch the event.');
  }
}

function unselect_node(node){
  node.removeClass('seleccionado');
  counting -= 1;
  try{remove_butaca(node)}catch (e){
    console.log('You must create a js function called remove_butaca to catch the event.');
  }
}

function find_node(id){
  return cy.$('#'+id);
}
