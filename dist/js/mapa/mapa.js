/**
 * Created by sergiow on 03-01-18.
 */
$(document).ready(function () {
    // DIBUJA LOS NODOS O ASIENTOS
    var cy = window.cy = cytoscape({
        container: $('.mapa'),

        minZoom: 12e-1,
        maxZoom: 0.1e1,
        boxSelectionEnabled: false,
        autounselectify: false,
        selectionType: 'single',
        autoungrabify: true, //avoid drag and drop

        layout: {
            name: 'preset',
            width: 6000,
            fit: true,
          },

        style: customStyle,
        elements: {
            nodes: test,
            edges: [],
          },
        ready: function () {
            //
          },
      });

    // ACCION TRAS CLIC SOBRE NODOS O ASIENTOS
    /*
    GET DATA FROM NODES
    var node     = evt.target;
    var id       = node.id();
    var label    = node.data().label;
    var type     = node.data().NodeType;
    var monto    = node.data().monto;
    var cantidad = node.data().cantidad;
    var moneda   = node.data().moneda;
    var id_db    = node.data().id_db;
    */

    cy.$('#1').on('tap', function (evt) {
        window.location.href = "super_vip.html";
      });

    cy.$('#2').on('tap', function (evt) {
        window.location.href = "vip.html";
      });

    cy.on('mouseover', '#1', function(event) {
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;

      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+cantidad;
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({

        content: mensaje,
        position: {
          my: 'bottom center', //Flecha del globo
          at: 'top center'// Globo


        },
        style: {
          classes: color,
          tip: {
            width: 16,
            height: 8,
          }
        },

        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        
        }
      }, event);
    });
    cy.on('mouseover', '#2', function(event){
      var node = event.target;
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+cantidad+'<br>';
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'bottom center', //Flecha del globo
          at: 'top center'// Globo
        },
        style: {
            classes: color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });
    cy.on('mouseover', '#3', function(event){
      var node = event.target;
      var node = event.target;
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+'disponible';
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'bottom center', //Flecha del globo
          at: 'top center',// Globo
          adjust: {
          y:30
        },
        },
        style: {
          classes: color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });
    cy.on('mouseover', '#4', function(event){
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+'disponible';
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'bottom center', //Flecha del globo
          at: 'top center',// Globo
          adjust: {
          y:130
        },
        },
        style: {
          classes:color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });
    cy.on('mouseover', '#5', function(event){
      var node = event.target;
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+'disponible';
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'left center', //Flecha del globo
          at: 'right center',// Globo
          adjust: {
            x:-50,
            y:-90
          }
        },
        style: {
          classes:color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });
    cy.on('mouseover', '#6', function(event){
      var node = event.target;
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+'disponible';
       color='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color='qtip-red';
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'right center', //Flecha del globo
          at: 'left center',// Globo
          adjust: {
            x:90,
            y:-90
          }
        },
        style: {
          classes: color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });
    cy.on('mouseover', '#7', function(event){
      var node = event.target;
      var node = event.target;
      var nombre =node.data().label;
      var monto =node.data().monto;
      var cantidad =node.data().cantidad;
      var estado=node.data().state;
      var mensaje;
      var color;
      if (estado==1) {
       mensaje=nombre + "<br>"+monto+'<br>'+cantidad+'<br>';
       color ='qtip-green';
      }else {
       mensaje=nombre + "<br>" +monto+'<br>'+'AGOTADO';
       color ='qtip-red'
      }
      node.qtip({
        content: mensaje,
        position: {
          my: 'bottom center', //Flecha del globo
          at: 'top center',// Globo
          adjust: {
          y:250
          }
        },
        style: {
          classes:color,
          tip: {
            width: 16,
            height: 8
          }
        },
        show: {
          event: event.type,
          ready: true
        },
        hide: {
          event: 'mouseout unfocus'
        }
      }, event);
    });

  });

function cerrar_par() {
  $('#butaca-par-info').hide('slow');
}

function reset_par() {
  cy.reset();
  cy.fit();
}
3
