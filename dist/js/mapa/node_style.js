var customStyle = [
  {
      selector: 'node',
      style: {
          label: 'data(label)',
          shape: 'polygon',
          'text-valign': 'center',
          'text-halign': 'center',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "super_vip"]',
      style: {
          width: 150,
          height: 40,
          'shape-polygon-points': super_vip,
          'text-valign': 'center',
          'background-color': '#CBC628',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "vip"]',
      style: {
          width: 160,
          height: 40,
          'shape-polygon-points': vip,
          'text-valign': 'center',
          'background-color': '#BA9243',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "gold"]',
      style: {
          width: 164,
          height: 40,
          'shape-polygon-points': gold,
          'text-valign': 'center',
          'background-color': '#5D2C7D',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "preferencia"]',
      style: {
          width: 400,
          height:400,
          'shape-polygon-points': preferencia,
          'text-valign': 'center',
          'background-color': '#251556',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "general"]',
      style: {
          width: 400,
          height: 400,
          'shape-polygon-points': general,
          'text-valign': 'center',
          'background-color': '#64262B',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "curva"]',
      style: {
          width: 400,
          height: 400,
          'shape-polygon-points': curva,
          'text-valign': 'bottom',
          'background-color': '#C1735A',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "campo"]',
      style: {
          width: 400,
          height: 400,
          'shape-polygon-points':campo,
          'text-valign': 'center',
          'background-color': '#599C55',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "butaca_libre"]',
      style: {
          width: 40,
          height: 40,
          shape: 'roundrectangle',
          'background-color': '#4AC366',
          'text-valign': 'center',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#000000',
        },
    },
  {
      selector: 'node[NodeType = "butaca_reservada"]',
      style: {
          width: 40,
          height: 40,
          shape: 'roundrectangle',
          'text-valign': 'center',
          'background-color': '#F6A300',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#000000',
        },
    },
  {
      selector: 'node[NodeType = "butaca_ocupada"]',
      style: {
          width: 40,
          height: 40,
          shape: 'roundrectangle',
          'text-valign': 'center',
          'background-color': '#FF0000',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#000000',
        },
    },
  {
      selector: 'node[NodeType = "butaca_inactiva"]',
      style: {
          width: 40,
          height: 40,
          shape: 'roundrectangle',
          'text-valign': 'center',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#989898',
        },
    },
  {
      selector: 'node[NodeType = "fila"]',
      style: {
          width:  40,
          height: 40,
          shape: 'roundrectangle',
          'text-valign': 'center',
          'background-color': '#3C82C0',
          'font-weight': 'bold',
          'font-size': '20px',
          color: '#ffffff',
        },
    },
  {
      selector: 'node[NodeType = "medios"]',
      style: {
          width: 600,
          height:690,
          shape: 'roundrectangle',
          'text-valign': 'center',
          
          'background-color': '#8EB4E3',
          'font-weight': 'bold',
          'font-size': '80px',
          color: '#000000',
        },
    },
    {
        selector: 'node[NodeType = "escenario"]',
        style: {
            
        },
    },
  {
      selector: ':selected',
      style: {
        'border-width': 2,
        'border-color': '#000000',
      },
    },
  {
      selector: '.title-escenario',
      style: {
        width: 125,
            height: 70,
            'background-width': 125,
            'background-height': 70,
            'background-fit': 'contain',
            'background-image': './../../dist/img/escenario.png',
            'background-color':'#cbc7c4'
      },
    },
  {
      selector: '.title-map',
      style: {
          'text-margin-y': '-145px',
          'text-halign': 'center',
          'font-size': '22px',
          'font-size': '20px',
          'text-wrap': 'wrap',
      },
    },
  {
      selector: '.title-asiento',
      style: {
          'text-margin-y': '-30px',
          'text-halign': 'center',
          'font-size': '50px',
          'text-wrap': 'wrap',
      },
    },

    {
        selector: '.title-preference',
        style: {
            'text-margin-y': '-100px',
            'text-margin-x': '110px',
            'font-size': '15px',
            'text-wrap': 'wrap',
        },
    },
    {
        selector: '.title-general',
        style: {
            'text-margin-y': '-100px',
            'text-margin-x': '-108px',
            'font-size': '15px',
            'text-wrap': 'wrap',
        },
    },
    {
        selector: '.title-campo',
        style: {
            'text-margin-y': '-30px',
            'text-margin-x': '0px',
            'font-size': '20px',
            'text-wrap': 'wrap',
        },
    },
    {
        selector: '.seleccionado',
        style: {
            'border-width': 5,
            'background-color': '#000',
            'border-color': '#000',
            'font-size': '30px',
            color: '#fff'
        },
    },

    {
        selector: '.mapa',
        style: {
            width: 320,
            height: 550,
            'background-width': 320,
            'background-height': 550,
            'background-fit': 'contain',
            'background-image': './../../dist/img/estadio.png',
            'background-color': 'white'
        }
    },

];
